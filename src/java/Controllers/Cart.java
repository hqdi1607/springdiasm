/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import DAOs.CartDAO;
import DAOs.Cart_itemDAO;
import DAOs.ProductDAO;
import DAOs.UserDAO;
import Models.Cart1;
import Models.Cart_item;
import Models.Product;
import Models.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Admin
 */
public class Cart extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Cart</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Cart at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String path = request.getRequestURI();
        if (path.endsWith("/Cart")) {
            request.getRequestDispatcher("Cart.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String product_id;
        String btn = null;
        String cookieName;
        int check = 0;
        btn = request.getParameter("order");
        if (btn != null) {
            product_id = request.getParameter("product_id");
            Cookie cookie = null;
            Cookie[] cookies = null;
            cookies = request.getCookies();
            if (cookies != null) {
                for (int i = 0; i < cookies.length; i++) {
                    cookie = cookies[i];
                    cookieName = cookie.getName();
                    if (cookieName.startsWith("product")) {
                        String[] s = cookieName.split("/");
                        String cookie_product_id = s[s.length - 1];
                        if (cookie_product_id.equals(product_id)) {
                            int quantity = Integer.parseInt(cookie.getValue());
                            quantity++;
                            Cookie product = new Cookie("product/" + product_id, String.valueOf(quantity));
                            product.setMaxAge(60 * 60 * 24);
                            response.addCookie(product);
                            check = 1;
                            response.sendRedirect("Cart");
                        }
                    }
                }
                if (check == 0) {
                    Cookie product = new Cookie("product/" + product_id, "1");
                    product.setMaxAge(60 * 60 * 24);
                    response.addCookie(product);
                    response.sendRedirect("Cart");
                }
            }
        }
        String btn2 = null;
        btn = request.getParameter("remove");
        if (btn != null) {
            cookieName = request.getParameter("cookie_name");
            Cookie cookie = null;
            Cookie[] cookies = null;
            cookies = request.getCookies();
            if (cookies != null) {
                for (int i = 0; i < cookies.length; i++) {
                    cookie = cookies[i];
                    if (cookie.getName().equals(cookieName)) {
                        cookie.setMaxAge(0);
                        response.addCookie(cookie);
                        response.sendRedirect("Cart");
                    }

                }
            }
        }
        String btn3 = null;
        int check3 = 0;
        btn = request.getParameter("clearCart");
        if (btn != null) {
            cookieName = request.getParameter("cookie_name");
            Cookie cookie = null;
            Cookie[] cookies = null;
            cookies = request.getCookies();
            if (cookies != null) {
                for (int i = 0; i < cookies.length; i++) {
                    cookie = cookies[i];
                    cookieName = cookie.getName();
                    if (cookieName.startsWith("product")) {
                        cookie.setMaxAge(0);
                        response.addCookie(cookie);
                        check3 = 1;
                    }
                }
            }
            if (check3 == 1) {
                response.sendRedirect("Cart");
            } else {
                response.sendRedirect("Cart");
            }
        }

        String btn4 = null;
        String time = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        int check4 = 0;
        int cart_id = 0;
        int user_id = 0;
        String user_username = null;
        String user_password = null;
        int is_admin = 0;

        btn = request.getParameter("checkOut");
        int checkProduct = 0;
        if (btn != null) {
            Cookie cookie = null;
            Cookie[] cookies = null;
            cookies = request.getCookies();
            if (cookies != null) {
                for (int i = 0; i < cookies.length; i++) {
                    cookie = cookies[i];
                    cookieName = cookie.getName();
                    if (cookieName.startsWith("product")) {
                        checkProduct = 1;
                    }
                }
                if (checkProduct == 0) {
                    HttpSession session = request.getSession();
                    session.setAttribute("message", "No product in cart");
                    request.getRequestDispatcher("Cart.jsp").forward(request, response);
                    session.setAttribute("message", "");
                }

                if (checkProduct == 1) {

                    for (int i = 0; i < cookies.length; i++) {
                        cookie = cookies[i];
                        cookieName = cookie.getName();
                        if (cookieName.startsWith("product")) {

                            String[] s = cookieName.split("/");
                            String cookie_product_id = s[s.length - 1];
                            int product_id2 = Integer.parseInt(cookie_product_id);
                            int quantity = Integer.parseInt(cookie.getValue());
                            String product_name = "";
                            String product_description = "";
                            int product_price = 0;
                            int product_stock = 0;
                            String product_manufacturer = "";
                            int product_category_id = 0;
                            int product_condition_id = 0;
                            String product_image = "";
                            Product p = new Product(product_id2, product_name, product_price,
                                    product_stock, product_description,
                                    product_manufacturer, product_category_id, product_condition_id, product_image);
                            p = ProductDAO.getProductByID(product_id2);
                            int updatedQuantity = p.getProduct_stock() - quantity;

                            if (updatedQuantity < 0) {
                                check4 = 1;
                                HttpSession session = request.getSession();
                                session.setAttribute("message", "Stock is not enough");
                                request.getRequestDispatcher("Cart.jsp").forward(request, response);
                                session.setAttribute("message", "");
                            } else if (check4 == 0) {

                                for (i = 0; i < cookies.length; i++) {
                                    cookie = cookies[i];
                                    cookieName = cookie.getName();
                                    if (cookieName.equals("user_username")) {
                                        String cookieValue = cookie.getValue();
                                        User u = new User();
                                        u = UserDAO.getUserByUsername(cookieValue);
                                        user_id = u.getUser_id();
                                        Cart1 c = new Cart1(cart_id, user_id, time);
                                        int count = CartDAO.addCart(c);
                                        if (count > 0) {
                                            c = CartDAO.getCartByTime(user_id, time);
                                            cart_id = c.getCart_id();
                                            for (i = 0; i < cookies.length; i++) {
                                                cookie = cookies[i];
                                                cookieName = cookie.getName();
                                                if (cookieName.startsWith("product")) {
                                                    s = cookieName.split("/");
                                                    cookie_product_id = s[s.length - 1];
                                                    product_id2 = Integer.parseInt(cookie_product_id);
                                                    quantity = Integer.parseInt(cookie.getValue());
                                                    Cart_item c_item = new Cart_item(cart_id, product_id2, quantity);
                                                    int count2 = Cart_itemDAO.addCart_item(c_item);
                                                    if (count2 > 0) {
                                                        p = new Product(product_id2, product_name, product_price,
                                                                product_stock, product_description,
                                                                product_manufacturer, product_category_id, product_condition_id, product_image);
                                                        p = ProductDAO.getProductByID(product_id2);

                                                        updatedQuantity = p.getProduct_stock() - quantity;
                                                        int count4 = ProductDAO.updateProductStock(product_id2, updatedQuantity);
                                                        if (count4 > 0) {
                                                            cookie.setMaxAge(0);
                                                            response.addCookie(cookie);

                                                        }

                                                    }
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            response.sendRedirect("ListProduct");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
