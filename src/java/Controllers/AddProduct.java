/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import DAOs.ProductDAO;
import DAOs.Product_categoryDAO;
import Models.Product;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author Admin
 */
public class AddProduct extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddProduct</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddProduct at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String path = request.getRequestURI();
        if (path.endsWith("/AddProduct")){
            ResultSet rs = Product_categoryDAO.getAllCategory();
            if (rs == null){
                
            }else{
                HttpSession session = request.getSession();
                session.setAttribute("allCategory", rs);
                request.getRequestDispatcher("AddProduct.jsp").forward(request, response);
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String add = null;
        int product_id = 0;
        int product_price, product_stock, product_category_id, product_condition_id;
        String product_name, product_description, product_manufacturer, product_image;
        String image2 = "";
        Hashtable params = new Hashtable();
        // Upload image to folder
        DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(fileItemFactory);
        try {
            // Create list to store information
            List<FileItem> fileItems = upload.parseRequest(request);
            // Store information is not image to list
            for (FileItem fileItem : fileItems) {
                if (fileItem.isFormField()) {
                    params.put(fileItem.getFieldName(), fileItem.getString("UTF-8"));

                }
            }
            // Get information of adminAddFood page
            add = (String) params.get("add");
            if (add != null) {
                // Get image file
                for (FileItem fileItem : fileItems) {
                    if (!fileItem.isFormField()) {
                        // Create a folder and upload image to folder
                        String nameimg = fileItem.getName();
                        if (!nameimg.equals("")) {
                            String folder = "upload";
                            String dirUrl = request.getServletContext()
                                    .getRealPath("") + "/" + folder;
                            File dir = new File(dirUrl);
                            if (!dir.exists()) {
                                dir.mkdir();
                            }
                            // Change file name to prevent same name problem
                            String nameimg2 = System.nanoTime() + "-" + nameimg;
                            String fileImg = dirUrl + "/" + nameimg2;
                            File file = new File(fileImg);
                            try {
                                // Upload file to folder
                                fileItem.write(file);                               
                                image2 = nameimg2;
                            } catch (Exception e) {                              
                                e.printStackTrace();
                            }
                        }
                    }
                }
                
                product_name = (String) params.get("product_name");
                product_price = Integer.parseInt((String) params.get("product_price"));
                product_stock = Integer.parseInt((String) params.get("product_stock"));
                product_description = (String) params.get("product_description");
                product_manufacturer = (String) params.get("product_manufacturer");
                product_category_id = Integer.parseInt((String) params.get("product_category_id"));
                product_condition_id = Integer.parseInt((String) params.get("product_condition_id"));
                product_image = image2;
                
                    
                    Product p = new Product(product_id, product_name, product_price, product_stock, product_description,
                            product_manufacturer, product_category_id, product_condition_id, product_image);
                    int count = ProductDAO.addProduct(p);
                    if (count > 0) {
                        response.sendRedirect("Admin");
                    } else {
                        response.sendRedirect("AddProduct");
                    }
                
            }
            
        } catch (FileUploadException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
