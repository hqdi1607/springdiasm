/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import static DAOs.UserDAO.getUserByUsernamePassword;
import Models.User;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Admin
 */
public class Login extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LoginController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LoginController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Cookie cookie = null;
        Cookie[] cookies = null;
        cookies = request.getCookies();
        int check = 0;
        String path = request.getRequestURI();
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                cookie = cookies[i];
                if (cookie.getName().equals("user_username")) {
                    check = 1;
                    response.sendRedirect("ListProduct");
                }
                if (cookie.getName().equals("admin_username")) {
                    check = 1;
                    response.sendRedirect("Admin");
                }
            }
            
            if (path.endsWith("/Login") && check == 0) {
                HttpSession session = request.getSession();
                session.setAttribute("message", null);
                request.getRequestDispatcher("Login.jsp").forward(request, response);
            }
        } else {
            path = request.getRequestURI();
            if (path.endsWith("/Login") && check == 0) {
                HttpSession session = request.getSession();
                session.setAttribute("message", null);
                request.getRequestDispatcher("Login.jsp").forward(request, response);
            }
        }
               
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String login = null;
        String user_username, user_password;
        login = request.getParameter("login");
        if (login != null) {
            user_username = request.getParameter("user_username");
            user_password = request.getParameter("user_password");
            User u = getUserByUsernamePassword(user_username, user_password);
            if (u == null){
                    HttpSession session = request.getSession();
                    session.setAttribute("message", "Your username or password is not correct!");
                    request.getRequestDispatcher("Login.jsp").forward(request, response);
                    session.setAttribute("message", "");
                }
            int is_admin = u.getIs_admin();
            if (is_admin == 0) {
                Cookie username = new Cookie("user_username", user_username);
                username.setMaxAge(60 * 60 * 24);
                response.addCookie(username);
                response.sendRedirect("ListProduct");
            }else
            if (is_admin == 1) {
                Cookie username = new Cookie("admin_username", user_username);
                username.setMaxAge(60 * 60 * 24);
                response.addCookie(username);
                response.sendRedirect("Admin");
                //request.getRequestDispatcher("Admin").forward(request, response);
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
