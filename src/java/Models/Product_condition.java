/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author Admin
 */
public class Product_condition {
    
    public Product_condition(){
        
    }
    
    private int product_condition_id;
    private String product_condition_name;

    public Product_condition(int product_condition_id, String product_condition_name) {
        this.product_condition_id = product_condition_id;
        this.product_condition_name = product_condition_name;
    }

    public int getProduct_condition_id() {
        return product_condition_id;
    }

    public void setProduct_condition_id(int product_condition_id) {
        this.product_condition_id = product_condition_id;
    }

    public String getProduct_condition_name() {
        return product_condition_name;
    }

    public void setProduct_condition_name(String product_condition_name) {
        this.product_condition_name = product_condition_name;
    }
    
    
}

    

