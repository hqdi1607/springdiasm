/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author Admin
 */
public class Product {
    
    public Product(){
        
    }
    
    private int product_id;
    private String product_name;
    private int product_price;
    private int product_stock;
    private String product_description;
    private String product_manufacturer;
    private int product_category_id;
    private int product_condition_id;
    private String product_image;

    public Product(int product_id, String product_name, int product_price, int product_stock, String product_description, String product_manufacturer, int product_category_id, int product_condition_id, String product_image) {
        this.product_id = product_id;
        this.product_name = product_name;
        this.product_price = product_price;
        this.product_stock = product_stock;
        this.product_description = product_description;
        this.product_manufacturer = product_manufacturer;
        this.product_category_id = product_category_id;
        this.product_condition_id = product_condition_id;
        this.product_image = product_image;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public int getProduct_price() {
        return product_price;
    }

    public void setProduct_price(int product_price) {
        this.product_price = product_price;
    }

    public int getProduct_stock() {
        return product_stock;
    }

    public void setProduct_stock(int product_stock) {
        this.product_stock = product_stock;
    }

    public String getProduct_description() {
        return product_description;
    }

    public void setProduct_description(String product_description) {
        this.product_description = product_description;
    }

    public String getProduct_manufacturer() {
        return product_manufacturer;
    }

    public void setProduct_manufacturer(String product_manufacturer) {
        this.product_manufacturer = product_manufacturer;
    }

    public int getProduct_category_id() {
        return product_category_id;
    }

    public void setProduct_category_id(int product_category_id) {
        this.product_category_id = product_category_id;
    }

    public int getProduct_condition_id() {
        return product_condition_id;
    }

    public void setProduct_condition_id(int product_condition_id) {
        this.product_condition_id = product_condition_id;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }
    
    
    
}
