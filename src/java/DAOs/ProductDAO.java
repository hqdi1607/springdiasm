/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOs;

import Connection.DBConnection;
import Models.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class ProductDAO {
    
    public static ResultSet getAllProduct(){
        try {
            Statement st = DBConnection.getConnection().createStatement();
            ResultSet rs = st.executeQuery("SELECT `product_id`, `product_name`, `product_price`,"
                    + " `product_stock`, `product_description`, `product_manufacturer`, "
                    + "`product_category_name`, `product_condition_name`, `product_image` "
                    + "FROM `product` "
                    + "NATURAL JOIN `product_category` "
                    + "NATURAL JOIN `product_condition`");
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static int addProduct(Product p){
        int rs = 0;
        Connection conn;
        try {
            conn = DBConnection.getConnection();
            PreparedStatement st = conn.prepareCall("INSERT INTO `product` "
                    + "(`product_id`, `product_name`, `product_price`, `product_stock`, "
                    + "`product_description`, `product_manufacturer`, `product_category_id`,"
                    + " `product_condition_id`, `product_image`) "
                    + "VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?);");
            
            st.setString(1, p.getProduct_name());
            st.setInt(2, p.getProduct_price());
            st.setInt(3, p.getProduct_stock());
            st.setString(4, p.getProduct_description());
            st.setString(5, p.getProduct_manufacturer());
            st.setInt(6, p.getProduct_category_id());
            st.setInt(7, p.getProduct_condition_id());
            st.setString(8, p.getProduct_image());
            rs = st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }
    
    public static Product getProductByID (int product_id){
        Product p = null;
        try {
            Connection conn = DBConnection.getConnection();
            PreparedStatement st = conn.prepareStatement("SELECT `product_id`, `product_name`,"
                    + " `product_price`, `product_stock`, `product_description`, `product_manufacturer`,"
                    + " `product_category_id`, `product_condition_id`, `product_image` FROM `product`"
                    + "NATURAL JOIN `product_category` "
                    + "NATURAL JOIN `product_condition`"
                    + " WHERE product_id = ?");
            st.setInt(1, product_id);
            ResultSet rs = st.executeQuery();            
            if(rs.next()){
                p = new Product();
                p.setProduct_id(product_id);
                p.setProduct_name(rs.getString("product_name"));
                p.setProduct_price(rs.getInt("product_price"));
                p.setProduct_stock(rs.getInt("product_stock"));
                p.setProduct_description(rs.getString("product_description"));
                p.setProduct_manufacturer(rs.getString("product_manufacturer"));
                p.setProduct_category_id(rs.getInt("product_category_id"));
                p.setProduct_condition_id(rs.getInt("product_condition_id"));
                p.setProduct_image(rs.getString("product_image"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return p;
    }
    
    public static int updateProductStock(int product_id, int quantity){
        int rs = 0;
        Connection conn;
        try {
            conn = DBConnection.getConnection();
            PreparedStatement st = conn.prepareCall("UPDATE `product` SET `product_stock`= ? "
                    + "WHERE `product_id`= ? ");
            
            st.setInt(1, quantity);
            st.setInt(2, product_id);
            
            rs = st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }
}
