/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOs;

import Connection.DBConnection;
import Models.Cart_item;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class Cart_itemDAO {
    public static int addCart_item(Cart_item c_item){
        int rs = 0;
        Connection conn;
        try {
            conn = DBConnection.getConnection();
            PreparedStatement st = conn.prepareCall("INSERT INTO `cart_item`"
                    + "(`cart_id`, `product_id`, `quantity`) "
                    + "VALUES (?, ?, ?) ");
            
            st.setInt(1, c_item.getCart_id());
            st.setInt(2, c_item.getProduct_id());
            st.setInt(3, c_item.getQuantity());
            
            rs = st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Cart_itemDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }
    
}
