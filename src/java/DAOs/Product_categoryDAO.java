/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOs;

import Connection.DBConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class Product_categoryDAO {
    public static ResultSet getAllCategory(){
        try {
            Statement st = DBConnection.getConnection().createStatement();
            ResultSet rs = st.executeQuery("SELECT `product_category_id`, "
                    + "`product_category_name` FROM `product_category`");
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(Product_categoryDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
