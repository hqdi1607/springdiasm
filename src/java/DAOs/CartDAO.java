/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOs;

import Connection.DBConnection;
import Models.Cart1;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class CartDAO {
    public static int addCart(Cart1 c){
        int rs = 0;
        Connection conn;
        try {
            conn = DBConnection.getConnection();
            PreparedStatement st = conn.prepareCall("INSERT INTO `cart`(`cart_id`, `user_id`, `time`) "
                    + "VALUES (NULL, ?, ?);");
            
            st.setInt(1, c.getUser_id());
            st.setString(2, c.getTime());
            
            rs = st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CartDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }
    
    public static Cart1 getCartByTime (int user_id, String time){
        Cart1 c = null;
        try {
            Connection conn = DBConnection.getConnection();
            PreparedStatement st = conn.prepareStatement("SELECT `cart_id`, `user_id`, `time` FROM `cart` "
                    + "WHERE `user_id` = ? AND `time` = ?");
            st.setInt(1, user_id);
            st.setString(2, time);
            ResultSet rs = st.executeQuery();            
            if(rs.next()){
                c = new Cart1();
                c.setCart_id(rs.getInt("cart_id"));
                c.setUser_id(user_id);
                c.setTime(time);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return c;
    }
    
    public static ResultSet getAllCart(){
        try {
            Statement st = DBConnection.getConnection().createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM `cart` "
                    + "NATURAL JOIN `cart_item` NATURAL JOIN `user` "
                    + "NATURAL JOIN `product` NATURAL JOIN `product_category` "
                    + "NATURAL JOIN `product_condition`;");
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(CartDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
