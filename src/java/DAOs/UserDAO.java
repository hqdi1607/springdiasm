/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOs;

import Connection.DBConnection;
import Models.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class UserDAO {

    public static User getUserByUsernamePassword(String user_username, String user_password) {
        User u = null;
        try {
            Connection conn = DBConnection.getConnection();
            PreparedStatement st = conn.prepareStatement("SELECT `user_id`, `user_username`, `user_password`, `is_admin` "
                    + "FROM `user` WHERE `user_username` = ? AND `user_password` = ? ;");
            st.setString(1, user_username);
            st.setString(2, user_password);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                u = new User();
                u.setUser_id(rs.getInt("user_id"));
                u.setUser_username(rs.getString("user_username"));
                u.setUser_password(rs.getString("user_password"));
                u.setIs_admin(rs.getInt("is_admin"));
            }
        } catch (SQLException ex) {           
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return u;
    }
    
    public static User getUserByUsername(String user_username) {
        User u = null;
        try {
            Connection conn = DBConnection.getConnection();
            PreparedStatement st = conn.prepareStatement("SELECT `user_id`, "
                    + "`user_username`, `user_password`, `is_admin` FROM `user` WHERE `user_username` = ? ;");
            st.setString(1, user_username);
            
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                u = new User();
                u.setUser_id(rs.getInt("user_id"));
                u.setUser_username(rs.getString("user_username"));
                u.setUser_password(rs.getString("user_password"));
                u.setIs_admin(rs.getInt("is_admin"));
            }
        } catch (SQLException ex) {           
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return u;
    }
}
