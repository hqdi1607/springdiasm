<%-- 
    Document   : Home
    Created on : May 31, 2022, 6:24:00 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <title>Home Page</title>
        <style type="text/css">

            body, html {
                height: 100%;
                margin: 0;
            }
        </style>
    </head>
    <body>
        <h1 style="margin-top: 0; padding:60px;font-size:50px;border-radius: 5px;text-align: left;background: #DCDCDC;">
            Welcome to Mobile Store FPT OJT 2021            
        </h1>
        <div class="d-flex justify-content-center">
        <a type="button" class="btn btn-primary" href="Login">Login</a>
        </div>
    </body>
</html>
