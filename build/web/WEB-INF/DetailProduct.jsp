<%-- 
    Document   : DetailProduct
    Created on : May 31, 2022, 6:23:31 PM
    Author     : Admin
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="Models.Product"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
              integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
                integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
        <style>
            .grid-container {
                display: grid;
                grid-template-columns: repeat(4, minmax(0, 1fr));
                grid-gap: 10px;
                padding: 10px;
            }
            .grid-item {
                background-color: rgba(255, 255, 255, 0.8);
                border: 1px solid rgba(0, 0, 0, 0.8);
                padding: 15px;
                font-size: 15px;                             

            }       

            header{
                background-color: #f0f0f0;
                float: left;
                width: 100%;
                height: 160px;

            }

            .head{
                font-size:20px;
            }
            .padding{
                padding:  20px 30px 1px 10px;

            }
            h1{
                font-size:50px;
            }
            .header{
                padding:  20px 30px 10px 50px;
            }
            .a_right{
                float: right;
                padding:  5px 10px 5px 10px;
                abc{
                    font-family: 200px;
                }
            }


        </style>
    </head>
    <body>
        <%
            Cookie cookie = null;
            Cookie[] cookies = null;
            cookies = request.getCookies();
            String username = "";
            if (cookies != null) {
                for (int i = 0; i < cookies.length; i++) {
                    cookie = cookies[i];
                    if (cookie.getName().equals("user_username")) {
                        username = cookie.getValue();
                    }
                }
            }
        %>
        <header>
            <div class="padding">
                <a class="btn btn-danger a_right" href="../Logout" role="button">Logout</a>
                <a href="../Cart" class="btn a_right" style="background-color: white">
                    <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> View cart
                </a>
                <button class="btn btn-primary a_right">Hello <%= username%></button>
            </div>
            <div class="header">
                <h1>Products</h1>
            </div>
        </header>
        <%
            Product p = (Product) session.getAttribute("singleProduct");
        %>
        <div class="container">
            <div class="row">
                <div class="col-xs-6">

                    <img height="400" src="../upload/<%= p.getProduct_image()%>">
                </div>
                <div class="col-xs-6">
                    <h2><%= p.getProduct_name()%></h2>
                    <p><%= p.getProduct_description()%></p>
                    <p><strong>Item Code: </strong><%= p.getProduct_id()%></p>
                    <p><strong>Manufacturer: </strong><%= p.getProduct_manufacturer()%></p>

                    <%
                        String category_name = "";
                        ResultSet rs = (ResultSet) session.getAttribute("allCategory");
                        while (rs.next()) {
                            if (rs.getInt("product_category_id") == p.getProduct_category_id()) {
                                category_name = rs.getString("product_category_name");
                            }
                        }

                    %>
                    <p><strong>Category: </strong><a><%= category_name%></a></p>
                    <p><strong>Available units in stock: </strong><%= p.getProduct_stock()%></p>
                    <p><%= p.getProduct_price()%> USD</p>
                    <a href="../ListProduct" class="btn btn-success">
                        <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Back
                    </a>
                    <form style="display: inline;" method="post" action="../Cart">
                        <input type="text" name="product_id" value="<%= p.getProduct_id() %>" style="display: none">
                        <button name="order" value="order" type="submit" class="btn btn-warning"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Order Now</button>                
                    </form>

                </div>
            </div>
        </div>

    </body>
</html>

