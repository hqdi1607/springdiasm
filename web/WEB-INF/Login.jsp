<%-- 
    Document   : Login
    Created on : May 31, 2022, 6:24:50 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>
        <style type="text/css">
            #login1{
                width:  400px;
                height: 200px;
                border: 1px solid grey;
                border-radius: 10px;
                text-align: center; 
                margin: auto;
                margin-bottom: 10px;

            }
            body, html {
                height: 100%;
                margin: 0;
            }
        </style>



    </head>
    <body>


        <h1 style="margin-top: 0; padding:60px;font-size:50px;border-radius: 5px;text-align: left;background: #DCDCDC;">
            Mobile Store
        </h1>
        <br>
        <%
            String message = "";
            message = (String) session.getAttribute("message");
            if (message == null){
                message = "";
            }
        %>
        <p style="color: red; text-align: center"><%= message%></p>
        
    <div id="login1">        
        <p style="padding:10px;text-align: left;border-radius: 5px; border: 0px solid grey;background: #DCDCDC;margin-top: 0px;">
            Please sign in
        </p>
        
        <form method="post" action="Login">

            <input type="text" required="required" name="user_username"
                   placeholder="User Name" style="height: 20px;width: 330px;padding-left: 15px;border-radius: 5px; border: solid 1px #e0e0e0;margin-bottom: 10px;font-family: sans-serif "title="Please enter username" size="50"> 
            <br>
            <input type="password" required="required" name="user_password"
                   placeholder="Password" style="height: 20px;width: 330px;padding-left: 15px;border-radius: 5px; margin-bottom: 15px; border: solid 1px #e0e0e0;" title="Please enter password" size="50">
            <br>
            <input type="submit" value="Login" class="btn hvr-hover" style="padding: 10px 16px;color: #FFFFFF;margin-bottom: 10px; font-size: 18px;border: 0px; border-radius: 6px;width: 300px;height: 40px; background-color:#45a049;"name="login">           
        </form>
        <a href="#" style="color: #000000; font-size: 2.5vw"><i class="fab fa-google-plus"></i></a>
    </div>
</body>
</html>

