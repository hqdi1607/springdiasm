<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <script src="DataTables/jquery-3.5.1.js" type="text/javascript"></script>
        <link href="DataTables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
        <script src="DataTables/jquery.dataTables.min.js" type="text/javascript"></script>

        <script>
            $(document).ready(function () {
                $('#example').DataTable();
            });
        </script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
              integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
                integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
        <style>

            header{
                background-color: #f0f0f0;
                float: left;
                width: 100%;
                height: 160px;

            }

            .head{
                font-size:20px;
            }
            .padding{
                padding:  20px 30px 1px 10px;

            }
            h1{
                font-size:50px;
            }
            .header{
                padding:  20px 30px 10px 50px;
            }
            .a_right{
                float: right;
                padding:  5px 10px 5px 10px;
                abc{
                    font-family: 200px;
                }
            }

        </style>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>       
        <%
            Cookie cookie = null;
            Cookie[] cookies = null;
            cookies = request.getCookies();
            String username = "";
            if (cookies != null) {
                for (int i = 0; i < cookies.length; i++) {
                    cookie = cookies[i];
                    if (cookie.getName().equals("admin_username")) {
                        username = cookie.getValue();
                    }
                }
            }
        %>
        <header>
            <div class="padding">
                <a class="btn btn-danger a_right" href="Logout" role="button">Logout</a>
                <button class="btn btn-primary a_right">Hello <%= username%></button>
            </div>
            <div class="header">
                <h1>Admin Page</h1>

            </div>
        </header>        
        <a href="AddProduct" class="btn btn-primary">Add Product</a>
        <a href="AllCart" class="btn btn-primary">All Cart</a>
        <br>
        <br>
        <table id="example" class="display" style="width:100%">
            <thead>
                <tr>
                    <th>Product Name</th>
                    <th>Price</th>
                    <th>Stock</th>
                    <th>Description</th>
                    <th>Manufacturer</th>
                    <th>Category Name</th>
                    <th>Condition Name</th>
                    <th>Image</th>
                    
                </tr>
            </thead>
            <tbody>
                <%
                    ResultSet rs = (ResultSet) session.getAttribute("allProduct");
                    while (rs.next()) {

                %>
                <tr>
                    <td style="text-align:center"><%= rs.getString("product_name")%></td>
                    <td style="text-align:center"><%= rs.getInt("product_price")%></td>
                    <td style="text-align:center"><%= rs.getInt("product_stock")%></td>
                    <td style="text-align:center"><%= rs.getString("product_description")%></td>
                    <td style="text-align:center"><%= rs.getString("product_manufacturer")%></td>
                    <td style="text-align:center"><%= rs.getString("product_category_name")%></td>
                    <td style="text-align:center"><%= rs.getString("product_condition_name")%></td>
                    <td style="text-align:center"><img height="100" src="upload/<%= rs.getString("product_image")%>"></td>
                    
                </tr>  
                <%
                    }
                %>
            </tbody>
            <tfoot>
                <tr>
                    <th>Product Name</th>
                    <th>Price</th>
                    <th>Stock</th>
                    <th>Description</th>
                    <th>Manufacturer</th>
                    <th>Category Name</th>
                    <th>Condition Name</th>
                    <th>Image</th>
                    
                </tr>
            </tfoot>
        </table>

    </body>
</html>

