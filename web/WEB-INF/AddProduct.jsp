<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
              integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
                integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>

        <style>

            header{
                background-color: #f0f0f0;
                float: left;
                width: 100%;
                height: 160px;

            }

            .head{
                font-size:20px;
            }
            .padding{
                padding:  20px 30px 1px 10px;

            }
            h1{
                font-size:50px;
            }
            .header{
                padding:  20px 30px 10px 50px;
            }
            .a_right{
                float: right;
                padding:  5px 10px 5px 10px;
                abc{
                    font-family: 200px;
                }
            }

        </style>
    </head>
    <body>
        <%
            Cookie cookie = null;
            Cookie[] cookies = null;
            cookies = request.getCookies();
            String username = "";
            if (cookies != null) {
                for (int i = 0; i < cookies.length; i++) {
                    cookie = cookies[i];
                    if (cookie.getName().equals("admin_username")) {
                        username = cookie.getValue();
                    }
                }
            }
        %>
        <header>
            <div class="padding">              
                <a class="btn btn-danger a_right" href="Logout" role="button">Logout</a>
                <button class="btn btn-primary a_right">Hello <%= username%></button>
            </div>
            <div class="header">
                <h1>Products</h1>
                <p>Add Product</p>
            </div>
        </header>     
        <p style="margin-bottom: 1px; margin-left: 20px; font-size: 25px">Add New Product</p>
        <hr style="margin-top:5px"> 

        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    <form class='form-horizontal' role='form'method="post" action="AddProduct" enctype="multipart/form-data">                      

                        <div style="display:inline-block">

                            <div class='col-md-12'style="margin-left: 50px" >

                                <div class='form-group'>
                                    <label class='control-label col-md-4'>Product Name</label>
                                    <div class='col-md-6'>
                                        <input type="text" required="required" name="product_name"
                                               placeholder="" title="Please enter product name">
                                    </div>
                                </div>
                                <div class='form-group'>
                                    <label class='control-label col-md-4'>Unit Price</label>
                                    <div class='col-md-6'>
                                        <input type="number" min="0" required="required" name="product_price"
                                               placeholder="" title="Please enter product price">
                                    </div>
                                </div>
                                <div class='form-group'>
                                    <label class='control-label col-md-4'>Units In Stock</label>
                                    <div class='col-md-6'>
                                        <input min="0" type="number" required="required" name="product_stock"
                                               placeholder="0" title="Please enter product in stock">
                                    </div>
                                </div>
                                <div class='form-group'>
                                    <label class='control-label col-md-4' for='id_comments'>Description</label>
                                    <div class='col-md-6'>
                                        <textarea type="text" required="required" name="product_description"
                                                  placeholder="" title="Please enter product description" rows='2'></textarea>
                                    </div>
                                </div>
                                <div class='form-group'>
                                    <label class='control-label col-md-4'>Manufacturer</label>
                                    <div class='col-md-6'>
                                        <input type="text" required="required" name="product_manufacturer"
                                               placeholder="" title="Please enter product manufacture">
                                    </div>
                                </div>
                                <div class='form-group'>
                                    <label class='control-label col-md-4'> Category</label>

                                    <select name="product_category_id"  style="margin-left:17px">
                                        <%
                                            ResultSet rs = (ResultSet) session.getAttribute("allCategory");
                                            while (rs.next()) {
                                        %>
                                        <option value="<%= rs.getString("product_category_id")%>"><%= rs.getString("product_category_name")%></option>
                                        <%
                                            }
                                        %>
                                    </select>   
                                </div>
                                <div class='form-group'>
                                    <label class='control-label col-md-4'>Condition</label>
                                    <div class='col-md-6'>

                                        <input type="radio" id="option1" name="product_condition_id" value="1" checked>
                                        <label>New</label>
                                        <input type="radio" id="option2" name="product_condition_id" value="2">
                                        <label>Old</label>
                                        <input type="radio" id="option3" name="product_condition_id" value="3">
                                        <label>Refurbished</label>

                                    </div>

                                </div>
                                <div  class='form-group'>

                                    <label class='control-label col-md-4'>Product Image File</label>

                                    <input id="imageUpload" type="file" accept=".png, .jpg" multiple="multiple" required="required"
                                           name="image" onchange="loadFile(event)"/>
                                    <script>
                                        var loadFile = function (event) {
                                            var output = document.getElementById('output');
                                            output.src = URL.createObjectURL(event.target.files[0]);
                                            output.onload = function () {
                                                URL.revokeObjectURL(output.src) // free memory
                                            }
                                        };
                                    </script>
                                </div>
                                <div class='form-group'>  

                                    <div class='col-md-offset-4'>
                                        <a class="btn btn-primary" href="Admin">Back</a>
                                        <input type="submit" value="Add Product" name="add" class="btn btn-primary">      
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-xs-6">
                    <img id="output" height="300"/>
                </div>
            </div>
        </div>
    </body>
</html>
