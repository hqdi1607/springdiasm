<%-- 
    Document   : Cart
    Created on : May 31, 2022, 6:22:54 PM
    Author     : Admin
--%>

<%@page import="Models.Product"%>
<%@page import="DAOs.ProductDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
              integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
                integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
        <style>
            .grid-container {
                display: grid;
                grid-template-columns: repeat(4, minmax(0, 1fr));
                grid-gap: 15px;
                padding: 5%;
            }
            .grid-item {
                background-color: rgba(255, 255, 255, 0.8);
                border: 1px solid #eeeeee;
                padding: 15px;
                font-size: 15px;                             

            }       

            header{
                background-color: #f0f0f0;
                float: left;
                width: 100%;
                height: 160px;

            }

            .head{
                font-size:20px;
            }
            .padding{
                padding:  20px 30px 1px 10px;

            }
            h1{
                font-size:50px;
            }
            .header{
                padding:  20px 30px 10px 50px;
            }
            .a_right{
                float: right;
                padding:  5px 10px 5px 10px;
                abc{
                    font-family: 200px;
                }
            }
            .pageBody{
                padding: 15% 10% 10% 10%;
            }


        </style>
    </head>
    <body>
        <%
            Cookie cookie = null;
            Cookie[] cookies = null;
            cookies = request.getCookies();
            String username = "";
            if (cookies != null) {
                for (int i = 0; i < cookies.length; i++) {
                    cookie = cookies[i];
                    if (cookie.getName().equals("user_username")) {
                        username = cookie.getValue();
                    }
                }
            }
        %>
        <header>
            <div class="padding">
                <a class="btn btn-danger a_right" href="Logout" role="button">Logout</a>
                <a href="Cart" class="btn a_right" style="background-color: white">
                    <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> View cart
                </a>
                <button class="btn btn-primary a_right">Hello <%= username%></button>
            </div>
            <div class="header">
                <h1>Cart</h1>
                <p>All the selected products in your cart</p>
            </div>
        </header>

        <div class="pageBody">

            <br>
            <%
                String message = "";
                message = (String) session.getAttribute("message");
                if (message == null) {
                    message = "";
                }
            %>
            <p style="color: red; text-align: center"><%= message%></p>
            
            <form style="display: inline;" method="post" action="Cart">
                <button name="clearCart" value="clearCart" type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Clear Cart</button>
            </form>
            <form style="display: inline;" method="post" action="Cart">
                <button name="checkOut" value="checkOut" type="submit" class="btn btn-success a_right"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Check Out</button>
            </form>
            <br>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Product</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Unit price</th>
                        <th scope="col">Price</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>

                    <%
                        int singlePrice = 0;
                        int totalPrice = 0;
                        String nameCookie;
                        for (int i = 0; i < cookies.length; i++) {
                            cookie = cookies[i];
                            nameCookie = cookie.getName();
                            if (nameCookie.startsWith("product")) {
                                String[] s = nameCookie.split("/");
                                String cookie_product_id = s[s.length - 1];
                                Product p = ProductDAO.getProductByID(Integer.parseInt(cookie_product_id));
                                singlePrice = p.getProduct_price() * Integer.parseInt(cookie.getValue());
                                totalPrice = totalPrice + singlePrice;
                    %>

                    <tr>
                        <td><%= p.getProduct_name()%></td>
                        <td><%= cookie.getValue()%></td>
                        <td><%= p.getProduct_price()%></td>
                        <td><%= singlePrice%></td>
                        <td>
                            <form style="display: inline;" method="post" action="Cart">
                                <input type="text" name="cookie_name" value="<%= cookie.getName()%>" style="display: none">
                                <button name="remove" value="remove" type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Remove</button>
                            </form>
                        </td>  
                    </tr>

                    <%
                            }
                        }
                    %>                        




                    <tr>
                        <td></td>
                        <td></td>
                        <td>Total Price</td>
                        <td><%= totalPrice%></td>
                        <td></td>                   
                    </tr>
                </tbody>

            </table> 
            <a href="ListProduct" class="btn btn-success">
                <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Continue Shopping
            </a>
        </div>
    </body>
</html>

